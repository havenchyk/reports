import React from 'react'

const Sidebar = () => {
    return (
        <aside className='sidebar'>
            <ul>
                <li>Some content1</li>
                <li>Some content2</li>
                <li>Some content3</li>
                <li>Some content4</li>
            </ul>
        </aside>
    )
}

export default Sidebar