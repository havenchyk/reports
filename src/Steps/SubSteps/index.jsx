import React, {Component} from 'react'
import ReportsStep from '../Step'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import ArrowDownwardIcon from 'material-ui/svg-icons/navigation/arrow-downward'
import {
  Step,
  Stepper,
  StepLabel,
  StepButton,
  StepContent
} from 'material-ui/Stepper'

export default class SubSteps extends Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.steps !== nextProps.steps) {
      this.setState({index: 0})
    }
  }


  state = {
    index: 0
  }

  getStepContent() {
    const {index} = this.state

    return JSON.stringify(this.props.steps[index], null, 2)
    // return JSON.stringify(this.state.steps[stepIndex], null, 2)
  }

  handleNext = () => {
    const {index} = this.state;
    const {steps} = this.props

    if (index < steps.length - 1) {
      this.setState({index: index + 1});
    }
  }

  handlePrev = () => {
    const {index} = this.state;

    if (index > 0) {
      this.setState({index: index - 1});
    }
  }

  _renderSteps() {
    const {index} = this.state
    const {steps} = this.props

    return (
        <Stepper activeStep={index} orientation='vertical'
                 connector={<ArrowDownwardIcon />}>
          {steps.map(step => {
            return <Step key={step.subStepResult}>
              <StepLabel>
                {step.subStepResult.slice(0, 5)}
              </StepLabel>

              <StepContent>
                <pre>{this.getStepContent()}</pre>

                {this._renderActions()}
              </StepContent>
            </Step>
          })}
        </Stepper>
    )
  }

  _renderActions() {
    const {steps} = this.props
    const {index} = this.state

    return (
        <div style={{marginTop: 24, marginBottom: 12}}>
          <FlatButton
              label="Previous substep"
              disabled={index === 0}
              onTouchTap={this.handlePrev}
              style={{marginRight: 12}}
          />
          <RaisedButton
              label='Next substep'
              disabled={index === steps.length - 1}
              primary={true}
              onTouchTap={this.handleNext}
          />
        </div>
    )
  }

  render() {
    const {steps} = this.props
    const {index} = this.state

    return <div>
      {this._renderSteps()}
    </div>
  }
}