import React, {Component} from 'react'
import * as API from '../api'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'

import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper'
import ArrowForwardIcon from 'material-ui/svg-icons/navigation/arrow-forward'
import ReportsStep from './Step'
import SubSteps from './SubSteps'

const keys = [
  'stepName',
  'stepType',
  'timestampStart',
  'timestampEnd',
  'stepResult'
]

export default class ReportsSteps extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showSubSteps: false
    }

    const id = this.props.params.reportId
    API.fetchReport(id).then(resp => {
      this.setState({
        steps: resp.reportSteps,
        index: 0
      })
    })

    this.handleNext = this.handleNext.bind(this)
    this.handlePrev = this.handlePrev.bind(this)
  }

  getStepContent(stepIndex) {
    return JSON.stringify(this.state.steps[stepIndex], null, 2)
  }

  handleNext() {
    const {index, steps} = this.state

    if (index < steps.length - 1) {
      this.setState({index: index + 1})
    }
  }

  handlePrev() {
    const {index} = this.state

    if (index > 0) {
      this.setState({index: index - 1})
    }
  }

  _renderSubSteps() {
    const {index, steps} = this.state
    const {subSteps} = steps[index]

    return <SubSteps steps={subSteps} />
  }

  _renderSteps() {
    const {index, steps} = this.state

    return (
        <Stepper activeStep={index} connector={<ArrowForwardIcon />}>
          {steps.map(step => {
            return <Step key={step.stepName}>
              <StepLabel>
                <ReportsStep step={step} />
              </StepLabel>
            </Step>
          })}
        </Stepper>
    )
  }

  render() {
    const {index, steps} = this.state

    if (index === undefined) return null

    const currentStep = this.state.steps[index]

    return (
        <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
          <div style={{marginTop: 24, marginBottom: 12}}>
            <FlatButton
                label="Previous Step"
                disabled={index === 0}
                onTouchTap={this.handlePrev}
                style={{marginRight: 12}}
            />
            <RaisedButton
                label='Next Step'
                disabled={index === steps.length - 1}
                primary={true}
                onTouchTap={this.handleNext}
            />
          </div>

          Steps:
          {this._renderSteps()}

          {this.state.showSubSteps && <div>
            Sub steps:
            {this._renderSubSteps()}
          </div>}

          {/*Current step:*/}
          {/*<pre>{this.getStepContent(index)}</pre>*/}

          <Table selectable={false}>
            {/*<TableHeader>*/}
              {/*<TableRow></TableRow>*/}
            {/*</TableHeader>*/}
            <TableBody displayRowCheckbox={false} showRowHover>
              {keys.map(key => (
                      <TableRow key={key}>
                        <TableRowColumn>{key}</TableRowColumn>
                        <TableRowColumn>{currentStep[key]}</TableRowColumn>
                      </TableRow>
                  )
              )}

              <TableRow key='substeps'>
                <TableRowColumn>sub steps</TableRowColumn>
                {console.log(currentStep.subSteps)}
                <TableRowColumn>{currentStep.subSteps.map(s => s.subStepResult).join('\n')}</TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </div>
    )
  }
}