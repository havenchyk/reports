import React, {Component} from 'react'

export default class Step extends Component {

  render() {
    const {step} = this.props

    return (
        <div>
          {step.stepName.slice(0, 5)}
        </div>
    )
  }
}