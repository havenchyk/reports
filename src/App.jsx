import React, { Component } from 'react'
import './App.css'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Main from './Main'
import Sidebar from './Sidebar'

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div className="container">
            {/*<Sidebar />*/}
            <Main>
              {this.props.children}
            </Main>
        </div>
      </MuiThemeProvider>      
    );
  }
}

export default App
