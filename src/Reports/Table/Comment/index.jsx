import React, {Component, PropTypes as PT} from 'react'
import FlatButton from 'material-ui/FlatButton'

export default class Comment extends Component {
  static propTypes = {
    report: PT.object.isRequired
  }

  state = {
    showInput: false
  }

  _toggleInput = () => {
    this.setState({
      showInput: !this.state.showInput
    })
  }

  _renderButtons() {
    return (
        <div>
          <FlatButton primary label='Save'/>
          <FlatButton secondary label='Discard'/>
        </div>
    )
  }

  _renderStub() {
    return <div onClick={this._toggleInput}>Add a comment!</div>
  }

  _renderComment() {
    return <div>{this.props.report.comment}</div>
  }

  render() {
    const {report} = this.props

    const content = report.comment ? this._renderComment(): this._renderStub()

    return (
        <div>
          {content}

          {this.state.showInput && this._renderButtons()}
        </div>
    )
  }
}