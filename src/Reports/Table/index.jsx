import React, {Component, PropTypes} from 'react'
import format from 'date-fns/format'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import { Link } from 'react-router'
import Comment from './Comment'
import './style.css'

const columns = [
  'reportName',
  'property1',
  'verdict',
  'property2',
  'timestampStart',
  'timestampEnd',
  'comment'
]

export default class ReportsTable extends Component {
  static propTypes = {
    reports: PropTypes.array
  }

  static defaultProps = {
    reports: []
  }

  state = {
    openedComments: new Set()
  }

  shouldComponentUpdate(nextProps) {
    return this.props.reports !== nextProps.reports
  }


  renderColumnName(dirtyName) {
    switch (dirtyName) {
      case 'reportName':
        return 'Name'
      case 'verdict':
        return 'Verdict'
      case 'timestampStart':
        return 'Start'
      case 'timestampEnd':
        return 'End'

      default:
        return dirtyName
    }
  }

  getClassName(report) {
    switch (report.verdict) {
      case 'In progress':
        return 'in_progress'

      default:
        return report.verdict.toLowerCase()
    }
  }

  renderCell(report, columnName) {
    switch (columnName) {
      case 'timestampStart':
      case 'timestampEnd':
        return format(report[columnName], 'DD-MM-YY HH:mm')
      case 'reportName':
        return (
            <Link to={`/${report.reportRunId}`}>
              {report.reportName}
            </Link>
        )

      case 'comment':
        return <Comment report={report} />

      case 'verdict':
        const className = columnName === 'verdict' ? this.getClassName(report) : ''

        return (
            <span className={className + ' verdict'}>
              {report.verdict}
            </span>
        )

      default:
        return report[columnName]
    }
  }

  render() {
    const {reports} = this.props

    if (!reports.length) return null

    return (
        <Table selectable={false}>
          <TableHeader displaySelectAll={false}
                       adjustForCheckbox={false}>
            <TableRow>
              {columns.map(columnName =>
                  <TableHeaderColumn key={columnName}>
                    {this.renderColumnName(columnName)}
                  </TableHeaderColumn>)
              }
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover> {/*showRowHover*/}
            {reports.map(report => {
              return <TableRow key={report.reportRunId}>
                {columns.map(columnName => {
                  return <TableRowColumn key={columnName}>
                    {this.renderCell(report, columnName)}
                  </TableRowColumn>
                })}
              </TableRow>
            })}
          </TableBody>
        </Table>
    )
  }
}