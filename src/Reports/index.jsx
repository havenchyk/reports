import React, {PropTypes, Component} from 'react'
import Table from './Table'
import * as API from '../api'
import debounce from 'lodash/debounce'

import TextField from 'material-ui/TextField'

let listOfIds = []
let reports = []
export default class Reports extends Component {
  constructor(props) {
    super(props)

    this.state = {}

    API.fetchReportsSummaries().then(response => {
      reports = response

      this.setState({reports})
    })
    API.fetchReportsRunsIds().then(ids => listOfIds = ids)
    // API.fetchReport()
    // API.fetchImage()
  }

  _filterReports = debounce((filter) => {
    console.log(filter)
    const filteredReports = reports.filter(report =>
        report.reportName.toLowerCase().includes(filter.toLowerCase()))

    this.setState({reports: filteredReports})
  }, 200)

  onChange = (e) => {
    const filter = e.target.value

    this._filterReports(filter)
  }

  render() {
    return (
        <div>
          <TextField hintText="Enter name here"
                     floatingLabelText="Search field by name"
                     floatingLabelFixed
                     name='filter'
                     onChange={this.onChange}/>
          <Table reports={this.state.reports}/>
        </div>
    )
  }
}