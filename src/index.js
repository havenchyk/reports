import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import Report from './Report'
import Steps from './Steps'
import ReportsList from './Reports'

injectTapEventPlugin();

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ReportsList}/>

      <Route path="/" component={ReportsList}/>
      <Route path="/:reportId" component={Steps}/>
    </Route>
  </Router>,
  document.getElementById('root')
)