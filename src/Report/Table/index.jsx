import React, {Component, PropTypes} from 'react'
import format from 'date-fns/format'
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table'

const columns = [
  'reportName',
  'reportRunId',
  'property1',
  'verdict',
  'property2',
  'timestampStart',
  'timestampEnd'
]

export default class ReportTable extends Component {
  static propTypes = {
    data: PropTypes.object
  }

  static defaultProps = {
    data: {}
  }

  renderColumnName(dirtyName) {
    switch (dirtyName) {
      case 'reportName':
        return 'Name'
      case 'reportRunId':
        return 'ID'
      case 'verdict':
        return 'Verdict'
      case 'timestampStart':
        return 'Start'
      case 'timestampEnd':
        return 'End'

      default:
        return dirtyName
    }

  }

  renderCell(report, columnName) {
    switch (columnName) {
      case 'timestampStart':
      case 'timestampEnd':
        return format(report[columnName], 'DD-MM-YYYY')

      default:
        return report[columnName]
    }
  }

  render() {
    const {data} = this.props

    if (!Object.keys(data).length) return null

    return (
        <Table selectable={false}>
          <TableBody displayRowCheckbox={false} showRowHover>
            {columns.map(columnName => {
              return (
                  <TableRow key={columnName}>
                    <TableRowColumn key={columnName}>
                      {this.renderColumnName(columnName)}
                    </TableRowColumn>
                    <TableRowColumn key={columnName}>
                      {this.renderCell(data, columnName)}
                    </TableRowColumn>
                  </TableRow>
              )
            })}
          </TableBody>
        </Table>
    )
  }
}