import React, {PropTypes, Component} from 'react'
import * as API from '../api'
import ReportTable from './Table'

export default class Report extends Component {
  constructor(props) {
    super(props)

    this.state = {}

    const id = this.props.params.reportId
    API.fetchReport(id).then(resp => this.setState({data: resp}))
  }


  render() {
    if (!this.state.data) return null

    return <ReportTable data={this.state.data} />
  }
}
