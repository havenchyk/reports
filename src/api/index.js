const endpoint = 'https://hidden-harbor-27149.herokuapp.com'

const fetch = (url, params) => {
  return window.fetch(`${endpoint}/${url}`)
      .then(resp => resp.json())
  // .then(resp => {
  //   console.log(`==============// ${url} //================`)
  //   console.log(resp)
  //   return resp
  // })
}



/**
 * /reportsSummaries
 Returns list of summaries for the reports.
 Please notice "verdict" property can be "Good", "Bad", "Inconclusive", "In progress", "Error".
 It would be great if these states be visible to the user by text and/or icon/row color/something else.
 If the report is "In progress" it will not have "timestampEnd".
 Do you recommend API to return empty string, null or omitting the property for that summery?
 */

export const fetchReportsSummaries = () =>
    fetch('reportsSummaries')

/**
 * /reportsRunsIDs
 Random ids matching the ones used in production.
 */
export const fetchReportsRunsIds = () =>
    fetch('reportsRunsIDs')


/**
 *
 * User should be able to select a report and view report details on a separate(?) page tab.
 * Sample random report details can be get here:
 * https://hidden-harbor-27149.herokuapp.com/report
 *
 * and the details for selected report app should get from:
 * /report/someValidUUID"
 * e.g:
 * https://hidden-harbor-27149.herokuapp.com/report/7fabeeeb-8422-4b2b-bd4b-9cd620683db7
 *  I added for development convenience to get one without providing runID
 *   /report/{runID}
 No changes.
 There are only few "reportStep" Types, and 95 % of them is type one.
 "timestampEnd" - same comment as above.
 "stepResult" - roughly 50% of steps return non empty result
 When viewing user should have a choice to set desired behaviour:
 to see stepResults only (with subSteps hidden) or everything right away.
 Plus See/hid all substeps or for a given step,
 "subStepType" there are a lot of them (20 - 30).
 The only properties common and always present for all subSteps are:
 "subStepType", "timestamp", and at least one of the string fields

 */
export const fetchReport = (id = '') =>
    fetch('report' + (id ? `/${id}` : ''))


/**

 "imageReferenceUrl" should not be visible by default, user must perform some action to view it.
 "imageToCompare" is present only if the report has some problems and there will be none most of the time.
 But if present user will rely on this image (and comparison to reference image) to investigate the problem.

 /image/{imageId} - multiple ids - in reportSteps
 */
export const fetchImage = (id) =>
    window.fetch(`${endpoint}/image` + (id ? `/${id}` : ''))
        .then(resp => resp.text())
        .then(resp => {
          console.log(resp)
          return resp
        })
