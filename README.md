This is a reports project for Michal.


Notes:
2) date range selector (with time selection) and filtering
4) create a column with comment
5) create a step flow with actions back and forward on the same page (same url)

Questions:
- what is `stepType` and `subStepType`

```
/reportsSummaries
Returns list of summaries for the reports.
Please notice "verdict" property can be "Good", "Bad", "Inconclusive", "In progress", "Error".
It would be great if these states be visible to the user by text and/or icon/row color/something else.
If the report is "In progress" it will not have "timestampEnd".
Do you recommend API to return empty string, null or omitting the property for that summery?

/reportsRunsIDs
Random ids matching the ones used in production.

/report
I added for development convenience to get one without providing runID

/report/{runID}
No changes.
There are only few "reportStep" Types, and 95 % of them is type one.
"timestampEnd" - same comment as above.
"stepResult" - roughly 50% of steps return non empty result
When viewing user should have a choice to set desired behaviour:
to see stepResults only (with subSteps hidden) or everything right away.
Plus See/hid all substeps or for a given step,
"subStepType" there are a lot of them (20 - 30).
The only properties common and always present for all subSteps are:
 "subStepType", "timestamp", and at least one of the string fields

"imageReferenceUrl" should not be visible by default, user must perform some action to view it.
"imageToCompare" is present only if the report has some problems and there will be none most of the time.
But if present user will rely on this image (and comparison to reference image) to investigate the problem.

/image/{imageId}
No changes.

/images
I added for development convenience to get one without providing imageID
```





```
Because it is POC the most important thing is how this SPA will look and feel.

There are public REST end points that can be used as a source of data for the POC.

https://hidden-harbor-27149.herokuapp.com/reportListSummary

Returns report list. User should be able to filter them by time slot, or by name containing a partial string.

User should be able to select a report and view report details on a separate(?) page tab.
Sample random report details can be get here:

https://hidden-harbor-27149.herokuapp.com/report


and the details for selected report app should get from: 
...com/report/someValidUUID" 
e.g:
https://hidden-harbor-27149.herokuapp.com/report/7fabeeeb-8422-4b2b-bd4b-9cd620683db7

I believe a form of collapsable accordion would fit best but I am open to suggestion.
Please notice imageReferenceUrl and imageToCompare, they will be taken with separate REST call
and you cannot predict size of images, they should be able to resize/fit
it side by side in the report so that user could compare them quickly when browsing the report but also if needed get a pop up(?) with origin full size images (or as big as can fit on the page).

Please use 
https://hidden-harbor-27149.herokuapp.com/image/someID
To get string with url of a random images to use.

The last part is the user should be able to run reports.
Here:
https://hidden-harbor-27149.herokuapp.com/reportList
A list of available reports can be acquired, properties are a default values but can be overwritten by a user using dropdown with an available preset values.
User should select all of available reports or any subset and request an execution with a button. App should send resulting json to a REST service for processing
```